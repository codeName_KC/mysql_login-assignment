const form = document.getElementById('login-form');

form.onsubmit = (e) => {
  e.preventDefault();
  
  const email = form.email.value; 
  const password = form.password.value;

  const data = {
    email,
    password,
  }

    fetch('http://localhost:4002/api/user/login', {
        method: 'POST',
        body: JSON.stringify(data), 
        headers:{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        
        }
    }).then(res => {
        return res.json()
    }).then((user) => {
  
      form.reset(); 
    }).catch( (error) => {
      console.log('Error:', error)
    });
}

