const form = document.getElementById('signup-form'); 
let Username; 

form.onsubmit = function(e) {
    e.preventDefault();
  
   
    const email = form.email.value; 
    const username = form.username.value;
    const password = form.password.value;
    const password2 = form.password2.value; 

    const user = {
        email,
        username,
        password,
        password2,
    }

    console.log(user)

  // Registering the user to the database: 
  fetch('http://localhost:4002/api/user/register', {
          method: 'POST',
          body: JSON.stringify(user), 
          headers:{
          'Content-Type': 'application/json'
          }
      }).then(res => {
        return res.json(); 
      }).then((newUser)=> {
        Username = newUser.username; 
        console.log(Username)
      }).catch(error => console.error('Error:', error));
    
    form.reset(); 
  }