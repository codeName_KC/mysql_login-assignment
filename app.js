const express = require("express");
const app = express();
//const mysql = require('mysql');
const mysql2 = require('mysql2');
const path = require('path');
const passport = require('passport');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
const sequelize = new Sequelize('mydb', 'root', 'admin', {
dialect: 'mysql'  
});
const models = require('./models'); 
const user  = require('./routes/api/user');


//Sync Database
models.sequelize.sync().then(function() {
 
    console.log('Nice! Database looks fine')
 
}).catch(function(err) {
 
    console.log(err, "Something went wrong with the Database Update!")
});


//Middlewares 

//Passport middleware
app.use(passport.initialize());

//Passport Config
require('./config/passport')(passport);

app.use(express.json());

app.use(express.static('./public/src/'))

app.use('/api/user', user);

// Listen on provided port, on all network interfaces.
const port = process.env.PORT || 4002;

app.listen(port, () => {
      console.log(`Listening on port ${port}`);
});
