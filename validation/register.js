const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validatorRegisterInput(data) {
    let errors = {};
   
        data.username = !isEmpty(data.username)
        ? data.username
        : '';
 
    data.email = !isEmpty(data.email)
        ? data.email
        : '';
    data.password = !isEmpty(data.password)
        ? data.password
        : '';
    data.password2 = !isEmpty(data.password2)
        ? data.password2
        : '';


    if (Validator.isEmpty(data.email)) {
        errors.email = 'Email field is required';
    }

    if (!Validator.isEmail(data.email)) {
        errors.email = 'Email field is required';
    }

    if (Validator.isEmpty(data.password)) {
        errors.password = 'Password is required';
    }

    if (!Validator.isLength(data.password, {
        min: 5
    })) {
        errors.password = 'Password must be atleast 5 characters';
    }

    return {errors, isValid: isEmpty(errors)}
}