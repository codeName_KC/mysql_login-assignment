"use strict";
module.exports = function(sequelize, DataTypes){
    var User = sequelize.define('User', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,

        },
        username: {
            type: DataTypes.STRING, 
            validate: {
                len: [2, 20]
            }
        },
        email: DataTypes.STRING,
        password: {
            type: DataTypes.STRING,
            validate: {
            len: {
                   args: [5],
                   msg: 'Password must be at least 5 characters'
                }
            }
        }
    });
    User.associate = function(models) {
        //associations can be defined here
       
    }
   return User; 
}; 


